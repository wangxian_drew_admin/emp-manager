import axios from "axios";

const http = axios.create({
    baseURL: 'http://localhost:8081',
    timeout: 1000 * 180,
    withCredentials: true
})

export default http
