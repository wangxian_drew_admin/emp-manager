import Vue from 'vue'
import Router from 'vue-router'
import Cookies from 'js-cookie'

Vue.use(Router)
export const pageRoutes = [
    {path: '/login', component: () => import('../views/Login'), name: 'login', meta: {title: '登录'}},
    {
        path: '/home', component: () => import('../views/Home'), name: 'home', meta: {title: '入口'}, children: [
            {path: '/index', component: () => import('../views/Index'), name: 'index', meta: {title: '首页'}},
            {path: '/user-info', component: () => import('../views/UserInfo'), name: 'userInfo', meta: {title: '个人中心'}},
            {path: '/register', component: () => import('../views/Register'), name: 'register', meta: {title: '我的登记'}},
            {path: '/work-time', component: () => import('../views/WorkTime'), name: 'userInfo', meta: {title: '工时查询'}},
        ]
    },
]


const router = new Router({
    mode: 'hash',
    scrollBehavior: () => ({x: 0, y: 0}),
    routes: pageRoutes
})
router.beforeEach((to, from, next) => {
    if (!Cookies.get('user')) {
        if (to.name !== 'login') {
            return next({name: 'login', replace: true})
        }
        return next()
    } else {
        if (to.name === 'login') {
            return next({name: 'home', replace: true})
        }
        next()
    }
})


export default router
