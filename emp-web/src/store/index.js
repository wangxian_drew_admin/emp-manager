import Vue from 'vue'
import Vuex from 'vuex'
import Cookie from 'js-cookie'
Vue.use(Vuex)
export default new Vuex.Store({
    namespaced: true,
    state: {
        userInfo: Cookie.get('user')?JSON.parse(Cookie.get('user')):{}
    },
    mutations: {
        login(state,user){
            state.userInfo = user
        }
    },
    getters: {}
})
