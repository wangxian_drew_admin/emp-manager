/**
 * 配置参考: https://cli.vuejs.org/zh/config/
 */
module.exports = {
  // 默认打开eslint效验，如果需要关闭，设置成false即可
  lintOnSave: false,
  productionSourceMap: false,
  devServer: {
    open: true,
    port: 8080,
    overlay: {
      errors: true,
      warnings: true
    },
    proxy: {
      '/': {
        target: 'http://localhost:8001/',
        changeOrigin: true,
      }
    }
  }
}
