package com.hkh.emp.core.constant;

/**
 * 常量
 * @author wangxian 2021/5/1
 */
public class Constant {
    /**
     * 注册用户默认密码
     */
    public static final String SIGN_UP_USER_DEFAULT_PASSWORD = "123456";
}
