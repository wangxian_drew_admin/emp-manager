package com.hkh.emp.core.result;

import lombok.Data;

/**
 * @author wangxian 2021/5/1
 */
@Data
public class JsonResult<T> {
    private String code;

    private String message;

    private T data;

    public static <T> JsonResult<T> ok(T data) {
        JsonResult<T> jsonResult = new JsonResult<>();
        jsonResult.setData(data);
        jsonResult.setCode("200");
        jsonResult.setMessage("操作成功");
        return jsonResult;
    }

    public static <T> JsonResult<T> error(String message){
        JsonResult<T> jsonResult = new JsonResult<>();
        jsonResult.setCode("500");
        jsonResult.setMessage(message);
        return jsonResult;
    }
}
