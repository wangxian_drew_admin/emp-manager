package com.hkh.emp.controller;

import com.hkh.emp.core.result.JsonResult;
import com.hkh.emp.entity.SysUser;
import com.hkh.emp.service.UserServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

/**
 * 用户API
 *
 * @author wangxian 2021/5/1
 */
@RequestMapping("/user")
@RestController
@Slf4j
public class UserController {
    @Autowired
    private UserServiceImpl userService;

    /**
     * 提示用户需登录
     * @return Json
     */
    @GetMapping("/login")
    public JsonResult<String> login(){
        return JsonResult.ok("请登录");
    }

    /**
     * 注册用户
     *
     * @param user 用户
     * @return Json
     */
    @PostMapping("/sign-up")
    public JsonResult<SysUser> signUp(@RequestBody SysUser user) {
        log.info("用户注册,user:{}", user);
        SysUser result = userService.signUp(user);
        return JsonResult.ok(result);
    }

    /**
     * 获取登录用户信息
     *
     * @return Json
     */
    @GetMapping("/current")
    public JsonResult<SysUser> getCurrentInfo() {
        SysUser sysUser = (SysUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return JsonResult.ok(sysUser);
    }

}
