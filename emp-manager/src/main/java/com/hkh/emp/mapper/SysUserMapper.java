package com.hkh.emp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hkh.emp.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @author wangxian 2021/5/1
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     * SELECT BY USERNAME
     * @param username username
     * @return SysUser
     */
    @Select("SELECT * FROM sys_user WHERE work_no = #{username} LIMIT 1")
    SysUser selectByUserName(@Param("username") String username);
}
