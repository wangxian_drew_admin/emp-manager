/*
 Navicat Premium Data Transfer

 Source Server         : locallhost
 Source Server Type    : MySQL
 Source Server Version : 80020
 Source Host           : localhost:3306
 Source Schema         : emp

 Target Server Type    : MySQL
 Target Server Version : 80020
 File Encoding         : 65001

 Date: 01/05/2021 23:06:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_team
-- ----------------------------
DROP TABLE IF EXISTS `sys_team`;
CREATE TABLE `sys_team` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `team_name` varchar(255) NOT NULL COMMENT '团队名字',
  `team_code` varchar(64) NOT NULL COMMENT '团队编码',
  `charge_user_id` bigint NOT NULL COMMENT '负责人用户ID',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='系统团队';

-- ----------------------------
-- Records of sys_team
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_team_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_team_user`;
CREATE TABLE `sys_team_user` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键Id',
  `team_id` bigint NOT NULL COMMENT '团队ID',
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='团队用户';

-- ----------------------------
-- Records of sys_team_user
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `work_no` varchar(50) NOT NULL COMMENT '工号',
  `real_name` varchar(20) NOT NULL COMMENT '姓名',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(255) DEFAULT NULL COMMENT '手机',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `is_login` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否登录过,用于判断是否需要重置密码,0-未登录,1-已登录',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `join_date` datetime DEFAULT NULL COMMENT '入司时间',
  `departure_date` datetime DEFAULT NULL COMMENT '离司时间',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态,1-正常,2-锁定',
  `create_time` timestamp NOT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_work_no` (`work_no`) USING BTREE COMMENT '工号'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户信息表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
BEGIN;
INSERT INTO `sys_user` VALUES (2, '00000001', '管理员', 'admin@emp.com', '15802560356', '$2a$10$fP0B0FkUew.Cj1UhR6cxaelJ0FqTC8NbGcDHdtWJWTQr.sJbzCjae', 0, NULL, NULL, NULL, 1, '2021-05-01 00:57:12', NULL);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
